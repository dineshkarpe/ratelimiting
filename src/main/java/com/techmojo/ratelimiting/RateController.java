package com.techmojo.ratelimiting;

import io.github.resilience4j.ratelimiter.RequestNotPermitted;
import io.github.resilience4j.ratelimiter.annotation.RateLimiter;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RateController {

    @GetMapping("/tenant")
    @RateLimiter(name = "tenantRateLimit", fallbackMethod = "tenantFallBack")
    public ResponseEntity tenant(@RequestParam(value = "name", defaultValue = "tenant") String name) {
        return ResponseEntity.ok().body("Response is successful: " + name);
    }

    public ResponseEntity tenantFallBack(String name, RequestNotPermitted ex) {
        return ResponseEntity.status(HttpStatus.TOO_MANY_REQUESTS)
                .body("Too many requests please retry after some time");
    }
}
